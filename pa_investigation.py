import glob
import json
import os
import argparse
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import time
from datetime import datetime


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--CATCHUP_DIR',
                        type=str,
                        help=('path to directory of catchup json files'),
                        required=False,
                        default='./catchup_oct_files/')
    parser.add_argument('--OUTPUT_DIR', type=str,
                        help=('path to directory where figures will be saved'),
                        required=False,
                        default='./figures/')
    return parser.parse_args()


def get_measurements_data(filename):
    '''
    takes a CSV with RDS data, resamples only valid data by lat_lon * 10^4 and by pollutant.
    :param filename: the csv file name of the RDS measurements
    :return: measurements: df. the results with the columns: 'station_id' (lat_lon as string), 'pollutant_name',
                            'time' (as hourly datetime), 'gt_value' (concentrations)

    '''
    measurements = pd.read_csv(filename)
    # take only valid data
    measurements = measurements[measurements.valid]
    # create a lat_lon column as station id
    measurements['station_id'] = (measurements.latitude * 10000).astype(int).astype(str) + '_' + (measurements.longitude * 10000).astype(int).astype(str)
    # filter only relevant columns
    measurements.rename(columns={'name': 'pollutant_name', 'value': 'gt_value'}, inplace=True)
    measurements = measurements[['station_id', 'pollutant_name', 'time', 'gt_value']]
    # resampling for each station and pollutant for a round hour
    measurements['time'] = pd.to_datetime(measurements['time'], utc=True)
    measurements.set_index('time', inplace=True)
    measurements = measurements.groupby(['station_id', 'pollutant_name']).resample('H').mean()
    measurements.reset_index(inplace=True)
    return measurements

def main(args):
    figures = args['OUTPUT_DIR']
    cur_dir = os.curdir
    # import measurements
    os.makedirs('figures', exist_ok=True)
    measurements_fn = f'{cur_dir}/bq-oct_measurements.csv'
    # filtering out invalid measurements
    measurements = get_measurements_data(measurements_fn)
    measurements['time'] = pd.to_datetime(measurements['time'], utc=True)
    time_catchup = time.time()
    # importing atchup then grouping and stats
    catchup_path = args['CATCHUP_DIR']
    os.makedirs(f'{catchup_path}', exist_ok=True)
    raw_catchup_data = glob.glob(catchup_path+'*')
    ranked_list = list()
    print(datetime.utcnow().isoformat(), 'Start loading catchup data')
    # todo: group chunks into functions
    #  execute flow function def prepare_pa_catchup():
    #  def import_catchup_data(path):
    #  def merge_and_filter():
    for rcd in raw_catchup_data:
        with open(rcd, 'rb') as f:
            # json file to DF
            j = json.load(f)['debug']
            json_df = pd.json_normalize(j)
            json_df = json_df.loc[json_df['pollutant_name'] == 'pm25']
            all_results_cols = [col for col in json_df.columns if col[:11] == 'all_results']
            exploded_df = json_df.explode(all_results_cols)
            t = os.path.basename(rcd).split('.')[0].replace('_', ':')
            exploded_df['time'] = t

        filtered_df = exploded_df.loc[~exploded_df["all_results.model_name"].isin(['ml_pd', 'ml_npd'])]
        df_PA = filtered_df[filtered_df['all_results.model_name'] == 'PurpleAir']
        filtered_df = filtered_df[filtered_df['station_id'].isin(df_PA['station_id'].unique())]
        filtered_df['time'] = pd.to_datetime(filtered_df['time'], utc=True)
        print(f'filtered: {filtered_df}')
        merged_df = pd.merge(left=filtered_df, right=measurements, how='left', on=['station_id', 'pollutant_name', 'time'])
        merged_df['gt_score'] = - abs(merged_df['gt_value'] - merged_df['all_results.value'])
        ranked_df = merged_df.copy()
        ranked_df.dropna(subset=['gt_value'], inplace=True)
        ranked_df['gt_score'] = - abs(ranked_df['gt_value'] - ranked_df['all_results.value'])
        ranked_df.loc[ranked_df["all_results.model_name"] == "PurpleAir", "all_results.score"] = 1
        ranked_df["rank"] = ranked_df.groupby(["station_id", "time"])["all_results.score"].rank("dense", ascending=False)
        ranked_list.append(ranked_df)
    # union of all catchup filtered results
    df_ranked = pd.concat(ranked_list)
    # pivot table and calculate difference between PA errors and other errors
    pivoted_df = ranked_df.loc[ranked_df['rank'].isin([1, 2, 3])].copy()
    pivoted_df = pd.pivot_table(data=pivoted_df,
                                index=['station_id', 'lat', 'lon', 'pollutant_name', 'gt_value', 'time'],
                                columns='all_results.model_name', values='gt_score').reset_index().sort_values(
        ['station_id', 'time'])
    # todo - figure out why there's no 'cams' column:
    # drop stations without PA contribution
    pivoted_df.dropna(subset=['PurpleAir'], inplace=True)
    # the difference in absolute value between PurpleAir RMSE and Persistence RMSE (=absolute error)
    pivoted_df['Diff'] = abs(pivoted_df['PurpleAir']).sub(abs(pivoted_df['persistence']), fill_value=0)
    print(datetime.utcnow().isoformat(), 'Writing CSVs of ranked Catchup predictions')
    # todo
    #  validate/fix - take only third place when cams in second place
    #  def ranking() def plotting(df)
    if 'cams' in pivoted_df.columns:
        cams_df = pivoted_df.copy().dropna(axis=0, subset=['cams'])
        cams_df.to_csv(f'{cur_dir}/cams_results.csv')
        pivoted_df = pivoted_df.dropna(subset=['PurpleAir', 'cams'], how='all')

    # split pa and second ranked models' results
    first_place_df = df_ranked.loc[ranked_df['rank'] == 1]  #pa_df
    second_place_df = df_ranked.loc[ranked_df['rank'] == 2]     #persistence_df

    pivoted_df.to_csv(f'{cur_dir}/pivoted_filtered_results.csv')
    df_ranked.to_csv(f'{cur_dir}/all_ranked_results.csv')
    time_elapsed = time.time() - time_catchup
    print(f'total elapsed time for data aggregation: {time_elapsed}')
    #describe all columns per station
    grouped_PA_errors = pa_df.groupby('station_id')
    all_columns_desc = grouped_PA_errors.describe()
    PA_stats = grouped_PA_errors.agg(max_err=('gt_score', 'max'),
                                     avg_err=('gt_score', 'mean')).reset_index()

    print(datetime.utcnow().isoformat(), 'Start plotting:')
    # plotting histograms and other stats
    # ploting describe() results
    col_list = [x for x in all_columns_desc.columns if x[0]=='Diff']
    fig = go.Figure()
    for c in col_list:
        fig.add_bar(x=all_columns_desc.index, y=all_columns_desc[c[0]][c[1]],
                    # marker=dict(color="LightSeaGreen"),
                    name=c[1])
    fig.update_layout(title_text="PA DIFF STATS (--> Diff = |PurpleAir - GT| - |Model2 - GT|)",
                          title_font_size=20, barmode='overlay')

    fig.show()
    fig.write_html(f'{figures}DIFF_describe_bars.html')


    # assign colors to negative and positive 'Diff' values:
    pivoted_df["legend"] = np.where(pivoted_df["Diff"] < 0, 'PA err larger', 'PA err smaller')
    colors = {'PA err larger': 'red',
              'PA err smaller': 'lightgreen'}

    #partial dataframe
    pdf = pivoted_df.copy()[['station_id', 'legend', 'Diff']]
    fig_diff_np = go.Figure()
    for label in pdf.legend.unique():
        fig_diff_np.add_trace(go.Histogram(
            x=pdf['station_id'],
            histnorm='percent',
            name=label,
            marker_color=colors[label],
            opacity=0.75
        ))
    fig_diff_np.show()
    # similarly with plotly.express
    hist = px.histogram(pivoted_df, x="station_id", color="legend", color_discrete_sequence=['indianred', 'lightgreen'])
    hist.update_layout(barmode='group')
    hist.show()
    hist.write_html(f'{figures}hist.html')

    # count negative and positive 'Diff's per station
    p = pivoted_df.groupby(['station_id', 'legend']).agg(
        count_diff=pd.NamedAgg(column="Diff", aggfunc="count"),
        lat=pd.NamedAgg(column="lat", aggfunc='mean'),
        lon=pd.NamedAgg(column="lon", aggfunc=np.mean)).reset_index()

    # plotting scattered counter values on map from mapbox, marker size is the counter (perhaps a horrible mistake)
    map_fig = px.scatter_mapbox(p, lat="lat", lon="lon", hover_name="station_id",
                            size='count_diff',
                             hover_data=["count_diff", 'station_id'], color='legend', zoom=2,
                            color_discrete_sequence=['indianred', 'lightgreen'],
                            # color_continuous_scale='RdBu_r',
                            # color_continuous_midpoint=0,
                            title='counters by categoty of difference between PA absolute error and second models (Persistence/ML) absolute error')
    # Anne's mapbox token
    tkn = 'pk.eyJ1IjoiYW5uZS1icnoiLCJhIjoiY2t2empscXB1NGo4aDJ3a2xtbGlldXJqaCJ9.LZmx_DPMXx5AKtf1z-20JQ'
    px.set_mapbox_access_token(tkn)
    map_fig.update_layout(mapbox_style='dark')
    map_fig.show()
    # saving the figure
    map_fig.write_html(f'{figures}worldmap.html')

if __name__=='__main__':
    args = vars(parse_args())
    main(args)

